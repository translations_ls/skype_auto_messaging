from skpy import SkypeEventLoop, SkypeNewMessageEvent
from datetime import datetime
from getpass import getpass
class AutoSkype(SkypeEventLoop):
    """
    Read the Skype event loop.
    """
    def __init__(self, username, password, message, end_hour):
        super(AutoSkype, self).__init__(username, password)
        self.answered_users = []
        self.message = message
        self.end_hour = end_hour
    
    def onEvent(self, event):
        """
        """
        if isinstance(event, SkypeNewMessageEvent) \
          and not event.msg.userId == self.userId \
          and event.msg.userId not in self.answered_users:
            now = datetime.now()
            if now.hour >= self.end_hour:
                print(now.hour)
                print(event.msg.userId)
                event.msg.chat.sendMsg(self.message)
                self.answered_users.append(event.msg.userId)
            

if __name__ == '__main__':
    """
    Testing file
    """
    username = input('Please insert your user name: ')
    password = getpass('Please insert the password: ')
    message = input('Please insert a message (Leave empty if you want default message): ')
    if not message:
        start_hour = input('Insert the hour that you start to work: ')
        end_hour = input('Insert the hour you leave the office (24 hours): ')
        team = input('Insert the Jira Project name: ')
        message = f'My office hours are from {start_hour} to {end_hour} CET. ' \
                        f'If you need anything related to scripts, please create a ticket on {team}. ' \
                        'If it is very urgent, please leave all the information required.' \
                        ' And I will process it the next working day ASAP.' 
        print(start_hour)
    else:
        end_hour = input('The hour should start the automatic messaging (24 hours): ')
    print(end_hour)
    print(message)
    sk_auto = AutoSkype(username, password, message, int(end_hour))
    sk_auto.loop()
